﻿open System
open System.IO
open System.Text

let mingle (p:string) (q:string) = 
    let rec mingleRec r s =
        match r with
        | [] -> []
        | rHead::rTail -> rHead :: List.head s :: mingleRec rTail (List.tail s)

    let toList (s:string) = [for c in s -> c]

    mingleRec (toList p) (toList q) |> List.toArray |> String


// Testing the algorithm from text file
let fileReader = new StreamReader ("/Users/sotomski/Developer/fsharptutorial/hackerrank/StringMingling_input.txt")
let input1 = fileReader.ReadLine()
let input2 = fileReader.ReadLine()
fileReader.Dispose()

String.length input1

let result = mingle input1 input2

String.length result

printfn "%A" result