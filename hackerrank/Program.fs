﻿open System

// Problem page: https://www.hackerrank.com/challenges/fp-list-replication

//let inputValues = System.Console.In.ReadToEnd().Split([|System.Environment.NewLine|], System.StringSplitOptions.RemoveEmptyEntries) |> Array.map (fun t-> int t) |> List.ofArray


//    let rec readAllInput reader = 
//        match reader() with
//        | null -> List.empty<int>
//        | line -> Int32.Parse( line ) :: (readAllInput reader)
//
//    let input = readAllInput Console.ReadLine
//    let delimiter = List.head input
//    let inputList = List.tail input
//
//    let rec filterList del list =
//        let headIfSmallerThanLimit listToTest = 
//            let head = List.head listToTest
//            match head < del with
//            | true -> Some( head )
//            | false -> None
//
//        match list with
//        | [] -> List.empty
//        | head :: tail -> match headIfSmallerThanLimit list with
//                          | None -> filterList del tail
//                          | Some(n) -> n :: filterList del tail
//
//    filterList delimiter inputList |> List.iter (fun x -> printfn "%i" x)
[<EntryPoint>]
let main (argv) =

    let inputValues = 
        System.Console.In.ReadToEnd().Split([|System.Environment.NewLine|], System.StringSplitOptions.RemoveEmptyEntries) 
        |> Array.map (fun t-> int t)
        |> List.ofArray
//    let inputValues = [8; 15; 22; 1; 10; 6; 2; 18; 18; 1]

    let removeOddElements inputList = 
        let isOdd x = ( x % 2 = 1 )
        let rec removeOddElementsRec list currentElementIndex =
            match list with
            | [] -> []
            | head::tail -> match isOdd currentElementIndex with
                            | true -> head::removeOddElementsRec tail (currentElementIndex + 1)
                            | false -> removeOddElementsRec tail (currentElementIndex + 1)

        removeOddElementsRec inputList 0
        

    removeOddElements inputValues |> List.iter (fun x -> printfn "%i" x)

    0