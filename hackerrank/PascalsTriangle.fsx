﻿//https://www.hackerrank.com/challenges/pascals-triangle

open System

// The value at the nth row and rth column of the triangle is equal to n!/(r!∗(n−r)!) where indexing starts from 0
let pascalFunction row column = row * column

