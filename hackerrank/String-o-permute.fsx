﻿// https://www.hackerrank.com/challenges/string-o-permute

// https://www.hackerrank.com/challenges/string-o-permute

//let swap (input:string) =
//    let rec swapList acc (list:char list) = 
//        match list with
//        | l when l.Length >= 2 -> 
//            let revHead = l |> List.take 2 |> List.rev
//
//            swapList (List.concat [ acc; revHead ]) (List.skip 2 l)
//        | _ -> acc
//
//    let result = []
//    [for c in input -> c]
//    |> swapList result
//    |> Array.ofList
//    |> String

open System
open System.IO

let swap (input:string) =
    let rec swapList (list:char list) = 
        match list with
        | [] -> []
        | (a :: b :: xs) -> b :: a :: (swapList xs)
        | _ -> []

    [for c in input -> c]
    |> swapList
    |> Array.ofList
    |> String

let performTests reader =
    let testCount = reader() |> Int32.Parse
    [for i in 1 .. testCount -> reader()]
    |> List.map (fun s -> swap s)
    |> seq

let printList listForPrint = listForPrint |> List.iter (fun s -> printfn "%s" s)

// Testing the algorithm from text file
let fileReader = new StreamReader ("/Users/sotomski/Developer/fsharptutorial/hackerrank/String-o-permute_input.txt")
let output = performTests fileReader.ReadLine
fileReader.Dispose()

let expectedOutput = seq {
    use sr = new StreamReader ("/Users/sotomski/Developer/fsharptutorial/hackerrank/String-o-permute_output.txt")
    while not sr.EndOfStream do
        yield sr.ReadLine ()
}

Seq.forall2 (fun el1 el2 -> el1 = el2) expectedOutput output