﻿open System

//let read_and_parse()=
//    let mutable arr = []
//    let mutable buff = Console.ReadLine()
//    while buff <> null do
//            arr <- Int32.Parse(buff)::arr
//            buff <- Console.ReadLine()
//    arr
//
//let main =    
//    let arr = read_and_parse()
//    printf "%A" <| f arr

type PolynomialElement = { A:float ; B:float }
type Polynomial = PolynomialElement list
type Range = { Left:float ; Right:float }

let polynomialFromCoeffs aCoeffs bCoeffs = List.zip aCoeffs bCoeffs |> List.map (fun x -> { A = fst x ; B = snd x } )


let applyPolynomial args poly = 
    let applyPolyForSingleCoefficient x = poly |> List.fold (fun sum p -> sum + (p.A * Math.Pow (x, p.B))) 0.0

    args |> List.map (fun x -> applyPolyForSingleCoefficient x)

let areaOfCircleOverRadiusList radiuses = radiuses |> List.map (fun x -> Math.PI * Math.Pow (x,2.0))

let voidFunction ar = ar

let limitedIntegralOverPolynomial func deltaX polynomialWithXs =
    polynomialWithXs
    |> func
    |> List.map (fun x -> x * deltaX)
    |> List.sum

let main() =
    let test func input expectedOutput =
        printfn "TEST CASE %A %f" input expectedOutput
        func input |> printfn "Output %A"
        printfn "Test result: %A" (Math.Abs(float expectedOutput - (func input)) < 0.1)

    let aCoeff = [1.0;2.0;3.0;4.0;5.0]
    let bCoeff = [6.0;7.0;8.0;9.0;10.0]
    let L = float 1
    let R = float 4
    let deltaX = 0.001

    let range = { Left = L ; Right = R }
    let xs = [ range.Left + deltaX .. deltaX .. range.Right ]
    let polyWithXs = polynomialFromCoeffs aCoeff bCoeff |> applyPolynomial xs
    polyWithXs |> limitedIntegralOverPolynomial voidFunction deltaX |> printfn "%A"
    polyWithXs |> limitedIntegralOverPolynomial areaOfCircleOverRadiusList deltaX |> printfn "%A"

main()

