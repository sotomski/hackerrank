﻿#load "SubsetSum.fs"

main Console.ReadLine

// Serious test case
#load "SubsetSumInput.fsx"
open System
open System.IO 
open SubsetSumInput

let stringReader = new StringReader(longInput)
printfn "%A" DateTime.Now
main stringReader.ReadLine
printfn "%A" DateTime.Now

// Simple test cases
[12UL; 10UL; 8UL; 4UL]
|> smallestSubsetsWithSumBiggerThan [ 4UL ]

[12UL; 10UL; 8UL; 4UL]
|> smallestSubsetsWithSumBiggerThan [ 13UL ]

[12UL; 10UL; 8UL; 4UL]
|> smallestSubsetsWithSumBiggerThan [ 30UL ]

[12UL; 10UL; 8UL; 4UL]
|> smallestSubsetsWithSumBiggerThan [ 130UL ]