﻿open System

[<EntryPoint>]
let main args =
    let n = int(Console.ReadLine())
    let a = Console.ReadLine().Split(' ') |> Array.map int64 |> Array.sort |> Array.rev
    let b = Array.scan (+) 0L a

    let FindIndex s =
        let mutable L = 0;
        let mutable R = b.Length-1
        let mutable res = -1;
        
        while (L <= R) do
            let M = (L+R)/2;
            if (b.[M] >= s) then
                res <- M
                R <- M-1
            else
                L <- M+1

        res

    let t = int(Console.ReadLine())
    [1..t]
        |> List.map(fun n -> int64(Console.ReadLine()))
        |> List.map FindIndex
        |> List.iter Console.WriteLine
    0
