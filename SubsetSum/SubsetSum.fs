﻿module SubsetSumCalculations

open System.Collections.Generic
open System.Linq
    
let smallestSubsetsWithSumBiggerThan (limits:uint64 list) sortedSet =
    //let memoize f =
    //    let dict = new Dictionary<_,_>()
    //    fun (limit:uint64) -> 
    //        match dict.Keys.Where(fun elem -> elem <= limit).DefaultIfEmpty(-1).Max() with
    //        | -1 -> 
    //            let temp = f sortedSet limit
    //            dict.Add(limit, temp)
    //            temp
    //        | _ -> 
    //            // Get non-zero starting conditions for f and start it
    //            dict.[keyToReturn]
    
    let findMinimalSubsetCountForLimit limit =
        let (resultSum, resultIdx) = 
            List.fold (fun (sum, idx) elem -> 
                if sum < limit then 
                    (sum + elem, idx + 1) 
                else 
                    (sum, idx)) 
             (0UL, 0) sortedSet

        if resultSum < limit && resultIdx = (List.length sortedSet) then
             (resultSum, -1)
        else
             (resultSum, resultIdx)

    limits
    |> List.mapi (fun idx limit -> findMinimalSubsetCountForLimit limit)


let rec readTestCases reader =
    match reader() with
    | null -> []
    | line -> (uint64 line) :: readTestCases reader

let executeTestCases set testCases =
    let sortedSet =
        set
        |> List.sortWith (fun x y -> 
            if x > y then
                -1
            else if x < y then
                1
            else
                0)

    smallestSubsetsWithSumBiggerThan testCases sortedSet
    

// Process input
let main (reader: unit -> string) =
    let inputListLength = reader()
    let inputList = reader().Split ([|" "|], System.StringSplitOptions.None)
                    |> Array.map (fun x -> uint64 x)
                    |> Array.toList
    let testCaseCount = reader()

    readTestCases reader
    |> executeTestCases inputList
    |> List.iter (fun (_, idx) -> printfn "%d" idx)

