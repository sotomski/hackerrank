﻿open System

let rec gcd x y =
    if y = 0L then x
    else gcd y (x % y)

let gcd2 a b =
     
    let rec inner r'' r' = 
        let step () = 
            let q = r'' / r'
            let r = r'' - q*r'
            r
        if r' = 0L then r''
        else inner r' (step())
 
    inner a b

let rec nww (input:int64 list) =
    let singleNww x y = (x / (gcd x y)) * y

    match input with
    | x::xs -> singleNww x (nww xs)
    | _ -> 1L

[<EntryPoint>]
let main argv = 
    let t = Console.ReadLine()
    Console.ReadLine().Split(' ') 
    |> Array.map int64 
    |> Array.toList
    |> nww
    |> printfn "%d"

    0 // return an integer exit code

